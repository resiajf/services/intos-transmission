FROM debian:stretch

RUN apt-get update && \
    apt-get install -y transmission-daemon && \
    apt-get clean && \
    rm -r /var/lib/apt && \
    rm -r /var/cache/apt && \
    mkdir -p /downloads && \
    mkdir -p /etc/transmission-daemon && \
    chmod 777 /downloads && \
    chmod 777 /etc/transmission-daemon

COPY settings.json /etc/default/transmission-daemon.settings.json
COPY td-start /

CMD /td-start
EXPOSE 9091 9092/udp
VOLUME /downloads /etc/transmission-daemon
