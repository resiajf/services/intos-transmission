# transmission

Holds the configuration of a torrent client via Transmission daemon. Uses Debian
9 as image base and over it, installs `transmission-daemon`. The file
`settings.json` stores the default configuration of the image.

## Mounts

### /downloading

This folder is for the downloading files (those that are being downloaded or
seeded) and they will be moved into `/downloaded` and removed from the torrent
list when they are completed (paused or the seed limit is reached).

### /downloaded

The folder where all the completed torrents will be moved to. It should be the
upload folder of the FTP.

### /etc/transmission-daemon

The configuration folder. Contains the `settings.json` and other files that
allows transmission to keep track of the torrents after a restart. If it is the
first time the folder is mounted, the default configuration (`settings.json`
from the repo) will be copied and then transmission will start. The file is
modified by the daemon, so any modification to it while the daemon is running
will be discarded. The file can be modified by the Web (located at ...)
or by stopping the service, modifying the file and starting again it.

 > It is adviced to copy the current `settings.json` into the repo as it serves
 as the default configuration in case of loss of data. Remember to recreate the
 image when the file is changed.

 > [Here][1] you can find documentation of what every option does in the config
 file.

## Dockefile

The file uses Debian 9 (aka stretch) as base and over it, install
`transmission-daemon` and `transmission-cli`, copies the default configuration
(that is `settings.json`) and a script that is executed when a torrent is
completed (paused or finished seeding).

Exposes the ports 9091/tcp for the Web Admin panel and 9092/udp for the seeding
port (helps to download and seed torrents). 9092 must be open in the firewall to
be accessible outside the local network.

To build the image, use `docker image build -t .../intos .`. This will use the
cache in you machine, so some steps won't be executed. If you want to update
transmission, you can try `docker image build --no-cache -t .../intos .`.


  [1]: https://github.com/transmission/transmission/wiki/Editing-Configuration-Files